import os
import sys
import random

from cluster import Cluster
from swarm import Swarm
from bird import Bird
from food import Food
from simulation import Simulation
from bird_factory import BirdFactory

if __name__ == "__main__":
    os.system('cls')

    print("Swarm Simulation\n")
    print("Construct Simulation Cenario ...")
    success_threshold = 10
    cenario_limits = [(0,0), (1000, 1000)]
    food = Food((340, 180))

    bird_count = 200
    bird_array = []

    for i in range(bird_count):
        position = (
            random.randint(cenario_limits[0][0], cenario_limits[1][0]), 
            random.randint(cenario_limits[0][1], cenario_limits[1][1]))

        bird = BirdFactory.create(food, position)

        bird_array.append(bird)

    swarm = Swarm(bird_array, success_threshold)

    simulation = Simulation(swarm, food)
    simulation.start()