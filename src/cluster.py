import math

class Cluster:
    
    def __init__(self, birds, threshold):
        self.birds = birds
        self.threshold = threshold
        self.velocity = (0,0)
        self.best = (0,0)
        self._compute_cluster()
    
    def _compute_cluster(self):
        sum_v = [0,0]
        best_cost = 9999999

        for bird in self.birds:
            sum_v[0] += bird.velocity[0]
            sum_v[1] += bird.velocity[1]

            v, p, d = bird.get_state()

            if d < best_cost:
                best_cost = d
                self.best = bird.velocity
        
        V = math.sqrt(math.pow(sum_v[0], 2) + math.pow(sum_v[1], 2))

        if V != 0:
            self.velocity = (sum_v[0] / V, sum_v[1] / V)
        
        else:
            self.velocity = (0, 0)