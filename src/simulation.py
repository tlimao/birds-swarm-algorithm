import sys
import math
import pygame
from pygame.locals import *

class Simulation:

    def __init__(self, swarm, food):
        self.swarm = swarm
        self.food = food

        pygame.init()
        self.screen = pygame.display.set_mode((1024, 768))
        self.fps = pygame.time.Clock()
        pygame.display.set_caption("Swarm Algorithm")
    
    def start(self):
        print("Start simulation ...")
        stop_flag = True

        while(stop_flag):
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

            self.swarm.update()

            self.screen.fill((0,0,0))

            for bird in self.swarm.birds:
                
                px = bird.position[0]
                py = bird.position[1]

                if px < 0 or px > 1024:
                    px = 1024 if (px < 0 ) else px if (px < 1024) else 0
                
                if py < 0 or py > 1024:
                    py = 768 if (py < 0 ) else py if (py < 768) else 0
                
                bird.position = (px, py)

                pygame.draw.circle(
                    self.screen, 
                    (255, 0, 0), 
                    [bird.position[0], bird.position[1]], 
                    3)

                # pygame.draw.circle(
                #     self.screen, 
                #     (125, 125, 125), 
                #     [bird.position[0], bird.position[1]], 
                #     50, 1)

                if not bird.stop:
                    v = math.sqrt(math.pow(bird.velocity[0], 2) + math.pow(bird.velocity[1], 2))
                    head = [bird.position[0] + bird.velocity[0] * 20 / v, bird.position[1] + bird.velocity[1] * 20 / v]
                    pygame.draw.line(
                        self.screen, 
                        (255, 0, 0), 
                        [bird.position[0], bird.position[1]], 
                        head)

            pygame.draw.circle(self.screen, (0, 255, 0), [self.food.x, self.food.y], 5)
            
            pygame.display.update()

            self.fps.tick(60)

