# Bird class
import math
import random

class Bird:

    def __init__(self, id, velocity, position, food, v_limit = 5):
        self.id = id
        self.velocity = velocity
        self.position = position
        self.food = food
        self.food_distance = None
        self.v_limit = v_limit
        self.stop = False

    def _update_food_distance(self):
        self.food_distance = math.sqrt(
            math.pow(self.position[0] - self.food.x, 2) + 
            math.pow(self.position[1] - self.food.y, 2))

        if self.food_distance < 10:
            self.stop = True
            self.velocity = (0,0)

    def _test_limits(self):
        V = math.sqrt(math.pow(self.velocity[0], 2) + math.pow(self.velocity[1], 2))

        # If velocity is greater then limit adjust
        scale = self.v_limit / V

        if V > self.v_limit:
            self.velocity = (self.velocity[0] * scale, self.velocity[1] * scale)

    def set_food(self, food):
        self.food = food

    def search(self):
        if not self.stop:
            # Update position
            self.position = (
                self.position[0] + self.velocity[0], 
                self.position[1] + self.velocity[1])

            # Update food distance
            self._update_food_distance()

    def update(self, cluster_velocity, cluster_best):
        if not self.stop:
            # Update bird velocity with cluster velocity and versor of better bird position
            rand_x = random.uniform(-3,3)
            rand_y = random.uniform(-3,3)
            self.velocity = (
                rand_x * 0.05 + self.velocity[0] * (1/self.food_distance) + cluster_velocity[0] * 0.05 + cluster_best[0] * 0.4,
                rand_y * 0.05 + self.velocity[1] * (1/self.food_distance) + cluster_velocity[1] * 0.05 + cluster_best[1] * 0.4)

            # Verify if bird velocity is greater then Limit Velocity
            self._test_limits()

    def get_state(self):
        return self.velocity, self.position, self.food_distance

    def __str__(self):
        return "ID: {id} Vel: {velocity} Pos: {position}".format(id=self.id, velocity=self.velocity, position=self.position)
