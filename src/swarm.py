# Swarm class
import math

from cluster import Cluster

class Swarm:
    cluster_radio = 50

    def __init__(self, birds, threshold):
        self.birds = birds
        self.threshold = threshold

    def _get_cluster(self, bird):
        nearest_birds = []
        b_x = bird.position[0]
        b_y = bird.position[1]

        for other_bird in self.birds:
            if other_bird.id != bird.id:
                ob_x = other_bird.position[0]
                ob_y = other_bird.position[1]

                distance = math.sqrt(
                    math.pow(b_x - ob_x,2) +
                    math.pow(b_y - ob_y,2))

                if distance <= self.cluster_radio:
                    nearest_birds.append(other_bird)

        if len(nearest_birds) != 0:
            return Cluster(nearest_birds, self.threshold)
        
        else:
            return None
    
    def update(self):
        flag_success = False

        for bird in self.birds:
            bird.search()

        for bird in self.birds:
            cluster = self._get_cluster(bird)

            if cluster != None:
                bird.update(cluster.velocity, cluster.best)
            
            else:
                bird.search()

    def __str__(self):
        return "Birds: {birds} Clusters: {cluster}".format(birds=len(self.birds), cluster=len(self.clusters))