import random

from bird import Bird

class BirdFactory:
    count = 0

    @classmethod
    def create(cls, food, position, qnt=1):
        cls.count += 1

        return Bird(
            id = cls.count,
            velocity = (random.uniform(-5,5), random.uniform(-5,5)),
            position = position,
            food = food,
            v_limit = 5)